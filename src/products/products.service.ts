import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product, ProductDocument } from './models/product.model';
import { Model, ObjectId } from 'mongoose';

@Injectable()
export class ProductsService {
  constructor(
    @InjectModel(Product.name) private productModel: Model<ProductDocument>,
  ) {}

  async create(createProductDto: CreateProductDto) {
    try {
      const newProduct = new this.productModel(createProductDto);

      return await newProduct.save();
    } catch (error) {
      throw error;
    }
  }

  async findAll(): Promise<Product[]> {
    try {
      return await this.productModel.find();
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: ObjectId): Promise<Product> {
    try {
      const product = await this.productModel.findById(id);

      if (!product) {
        throw new NotFoundException('Resource not found');
      }

      return product;
    } catch (error) {
      throw error;
    }
  }

  async update(id: ObjectId, updateProductDto: UpdateProductDto) {
    try {
      await this.findOne(id);

      return await this.productModel.findByIdAndUpdate(id, updateProductDto);
    } catch (error) {
      throw error;
    }
  }

  async remove(id: ObjectId) {
    try {
      await this.findOne(id);

      return await this.productModel.findByIdAndRemove(id);
    } catch (error) {
      throw error;
    }
  }
}
