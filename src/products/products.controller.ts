import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UnprocessableEntityException,
  Render,
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { ObjectId, isValidObjectId } from 'mongoose';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Get('page')
  @Render('index')
  async getDataInPage() {
    try {
      const products = await this.productsService.findAll();

      return {
        products,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post()
  async create(@Body() createProductDto: CreateProductDto) {
    try {
      return await this.productsService.create(createProductDto);
    } catch (error) {
      throw error;
    }
  }

  @Get()
  async findAll() {
    try {
      return await this.productsService.findAll();
    } catch (error) {
      throw error;
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: ObjectId) {
    try {
      if (!isValidObjectId(id)) {
        throw new UnprocessableEntityException('Bad object id');
      }

      return await this.productsService.findOne(id);
    } catch (error) {
      throw error;
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: ObjectId,
    @Body() updateProductDto: UpdateProductDto,
  ) {
    try {
      if (!isValidObjectId(id)) {
        throw new UnprocessableEntityException('Bad object id');
      }

      // If whole request body is empty, throw error
      if (JSON.stringify(updateProductDto) === '{}') {
        throw new UnprocessableEntityException(
          'Please provide any field values to update',
        );
      }

      return await this.productsService.update(id, updateProductDto);
    } catch (error) {
      throw error;
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: ObjectId) {
    try {
      if (!isValidObjectId(id)) {
        throw new UnprocessableEntityException('Bad object id');
      }

      return await this.productsService.remove(id);
    } catch (error) {
      throw error;
    }
  }
}
